import calc
import pytest


def test_add():
    # проверка int:
    assert calc.add(5,3) == 8
    # проверка float:
    assert calc.add(5.1, 3.2) == 8.3
    # проверка передачи string:
    with pytest.raises(TypeError):
        calc.add(5,"3")
    # проверка отсутсвия аргумента:
    with pytest.raises(TypeError):
        calc.add(5, )
    # проверка передачи списка:
    with pytest.raises(TypeError):
        calc.add(5, [2, 9])


def test_sub():
    # проверка int:
    assert calc.sub(5,3) == 2
    # проверка float:
    assert calc.sub(5.5, 3.2) == 2.3
    # проверка передачи string:
    with pytest.raises(TypeError):
        calc.sub(5.5, "3.2")
    # проверка отсутсвия аргумента:
    with pytest.raises(TypeError):
        calc.sub()
    # проверка передачи списка:
    with pytest.raises(TypeError):
        calc.sub(5.5, [1, 2])


def test_mul():
    # проверка int:
    assert calc.mul(5,3) == 15
    # проверка float:
    assert calc.mul(5.5, 3) == 16.5
    # проверка передачи string:
    with pytest.raises(TypeError):
         calc.mul(5.1, "3")
    # проверка отсутсвия аргумента:
    with pytest.raises(TypeError):
         calc.mul()
    # проверка передачи списка:
    with pytest.raises(TypeError):
         calc.mul(5.1, [5, 3])


def test_div():
    # проверка int:
    assert calc.div(6,3) == 2
    # проверка float:
    assert calc.div(6.3, 3) == 2.1
    # проверка передачи string:
    with pytest.raises(TypeError):
        calc.div(6.3, "3")
    # проверка отсутсвия аргумента:
    with pytest.raises(TypeError):
        calc.div()
    # проверка передачи списка:
    with pytest.raises(TypeError):
        calc.div(6.3, [1, 2])
    # проверка деленния на 0:
    with pytest.raises(ZeroDivisionError):
        calc.div(6.3, 0)



